class Book
  attr_reader :title

  def initialize
    @self = self
  end

  def title=(title)
    @title = title.split(" ").each_with_index.map{|x,y|
      if ((x == "and" || x == "in" || x == "or" || x == "of" || x == "the" || x == "a" || x == "an") && y != 0)
        x
      else
        x.capitalize
      end
    }.join(" ")
  end
end

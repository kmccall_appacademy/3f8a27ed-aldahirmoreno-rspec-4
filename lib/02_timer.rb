class Timer
  attr_reader :seconds

  def initialize
    @seconds = 0
  end

  def seconds=(seconds = 0)
    @seconds  = seconds
  end

  def time_string
    @minutes = 0
    @hours = 0
    secdup = seconds
    while secdup > 60
      secdup -= 60
      @minutes += 1
      if @minutes == 60
        @hours += 1
        @minutes = 0
      end
    end
    trial_str = "#{@hours}:#{@minutes}:#{secdup}"
    trial_str.split(":").map{|x|
      if x.length == 1
        (x + "0").reverse
      else
        x
      end
    }.join(":")
  end
end

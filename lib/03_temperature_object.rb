class Temperature
  attr_reader :fahrenheit, :celsius

  def initialize(entry={})
    @fahrenheit = entry[:f]
    @celsius = entry[:c]
  end

  def in_fahrenheit
    if fahrenheit == nil
      (celsius * 9.0 / 5.0) + 32
    else
      fahrenheit
    end
  end

  def in_celsius
    if celsius == nil
      (fahrenheit - 32) * (5.0 / 9.0)
    else
      celsius
    end
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end

end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end

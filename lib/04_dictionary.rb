class Dictionary
  attr_reader :entries, :keywords

  def initialize
    @entries = {}
    @keywords = []
  end

  def add(entry)
    if entry.class != Hash
      @entries[entry] = nil
      @keywords.push(entry)
    else
      @entries = entries.merge(entry)
      @keywords.push(entry.keys)
      @keywords = @keywords.flatten.sort
    end
  end

  def include?(entry)
    if keywords.include?(entry)
      true
    else
      false
    end
  end

  def find(word)
    found_hash = {}
    keywords.each{|x| found_hash[x] = entries[x] if x.include?(word) }
    found_hash
  end

  def printable
    arr = []
    keywords.each{|x|
      arr << "[#{x}] \"#{entries[x]}\""
    }
    arr.join("\n")
  end
end
